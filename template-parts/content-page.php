<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VictorySistersV2
 */

?>

<article id="post-<?php the_ID(); ?>" class="slide dado blue-wall" <?php post_class(); ?>>
<?php the_title(); ?>
	<div class="entry-content">
		<div class="slide-background"></div>
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'victorysistersv2' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
