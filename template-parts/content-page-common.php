<div class="slide-background"></div>
<div class="slide-wrapper">
    <div class="entry-content slide-content">
        <?php
        the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'victorysistersv2' ),
            'after'  => '</div>',
        ) );
        ?>
    </div>
</div>