<?php
    /*
        Template Name: Slide: Blue wall
    */
    get_header();
    ?>

<?php
    while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/content', 'blue-wall' );

    endwhile; // End of the loop.
?>

<?php get_footer(); ?>