<?php
    /*
        Template Name: Single Page Theme Page
    */
    get_header();

    $currentID = get_the_ID();
    $args = array("post_type" => "page", "order" => "ASC", "orderby" => "menu_order", 'post__not_in' => array($currentID));
    $the_query = new WP_Query($args);
    ?>
<?php if(have_posts()):while($the_query->have_posts()):$the_query->the_post(); ?>
<?php get_template_part( 'template-parts/content', substr(get_page_template_slug(), 0, -4) ); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>