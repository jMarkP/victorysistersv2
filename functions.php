<?php
/**
 * VictorySistersV2 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package VictorySistersV2
 */

if ( ! function_exists( 'victorysistersv2_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function victorysistersv2_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on VictorySistersV2, use a find and replace
		 * to change 'victorysistersv2' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'victorysistersv2', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		// add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'victorysistersv2' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'victorysistersv2_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'victorysistersv2_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function victorysistersv2_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'victorysistersv2_content_width', 640 );
}
add_action( 'after_setup_theme', 'victorysistersv2_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function victorysistersv2_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'victorysistersv2' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'victorysistersv2' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'victorysistersv2_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function victorysistersv2_scripts() {
	wp_enqueue_style( 'victorysistersv2-style', get_stylesheet_uri() );

	wp_enqueue_script( 'victorysistersv2-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'victorysistersv2-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'victorysistersv2_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function custom_add_google_fonts() {
	wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700', false );
}
add_action( 'wp_enqueue_scripts', 'custom_add_google_fonts' );

if(get_page_by_title("Home") == null)
{
    $post = array(
        "post_title" => "Home",
        "post_status" => "publish",
        "post_type" => "page",
        "menu_order" => "-100",
        "page_template" => "single-page-theme.php"
    );

    wp_insert_post($post);

    $home_page = get_page_by_title("Home");
    update_option("page_on_front",$home_page->ID);
    update_option("show_on_front","page");
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/jMarkP/victorysistersv2',
	__FILE__,
	'victorysisters2'
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'tFp2cB2kVdG9UtuBCm',
	'consumer_secret' => 'Qz5YqVWe7RNLeNmhJ8zpkYygkxWD2sdM',
));
$myUpdateChecker->setBranch('master');

function vs_include_content($pid) {
	$thepageinquestion = get_post($pid);
	$content = $thepageinquestion->post_content;
	$content = apply_filters('the_content', $content);
	echo $content;
}

function add_my_media_controls($wp_customize) {
	$wp_customize->add_section('site-media', array(
		'title' => 'Site media',
		'description' => 'Add sound/media to your website',
		'capability' => 'edit_theme_options'
	));

	$wp_customize->add_setting('sample-music', array(
		'type' => 'theme_mod',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'absint'
	));

	$wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'sample-music', array(
		'section' => 'site-media',
		'label' => 'Sample music',
		'mime_type' => 'audio'
	)));
}
add_action('customize_register', 'add_my_media_controls');

function echo_theme_sound() {
	$id = get_theme_mod('sample-music');
	if ($id != 0) {
		// Display the tag
		$attr = array(
			'src' => wp_get_attachment_url($id)
		);
		$metadata = get_post_meta($id, "_wp_attachment_metadata");
		$title = get_the_title( $id );
	// 	echo '
	// <div class="samples listen-container">
	// 	<img class="listen" title="Take a listen!" src="' . get_template_directory_uri() . '/img/listen.png" />
	// 	<div class="audio-sample">
	// 		<span class="title">' . $title . '</span>
	// 		<div class="player"> 
	// 			' . wp_audio_shortcode($attr) . '
	// 		</div>
	// 	</div>
	// </div>';
	echo '
	<div class="samples listen-container">
		<img class="listen" title="Take a listen!" src="' . get_template_directory_uri() . '/img/listen.png" />
		<div class="audio-sample">
			<div class="player"> 
				' . wp_audio_shortcode($attr) . '
			</div>
		</div>
	</div>';
	}
}
