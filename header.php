<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VictorySistersV2
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="theme-color" content="#e1d2a8"/>
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class('top-nav'); ?>>

	<div class="slides">
		<header class="site-header">
			<div class="header-content">
				<div class="site-branding">
					<div class="masthead">
						<img class="vs-logo" title="The Victory Sisters - Wartime Vocal Trio" src="<?php echo get_template_directory_uri() . '/img/logo-with-tape.png'; ?>" />
					</div>
					<?php echo_theme_sound() ?>
				</div>

				<?php
					wp_nav_menu( array(
						'theme_location'  => 'menu-1',
						'menu_id'         => 'site-navigation',
						'container'       => 'nav',
						'container_class' => 'main-navigation',
						'container_id'    => 'main-navigation'
					) );
				?>
			</div>

		</header>
