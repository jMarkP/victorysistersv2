<?php
    /*
        Template Name: Slide: Grey Wallpaper
    */
    get_header();
    ?>

<?php
    while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/content', 'grey-wallpaper' );

    endwhile; // End of the loop.
?>

<?php get_footer(); ?>