<?php
    /*
        Template Name: Slide: Gemetric wallpaper
    */
    get_header();
    ?>

<?php
    while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/content', 'geo-wall' );

    endwhile; // End of the loop.
?>

<?php get_footer(); ?>